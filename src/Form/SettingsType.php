<?php

namespace App\Form;

use App\Entity\Settings;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends ProfileType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Settings::class,
        ]);
    }
}
